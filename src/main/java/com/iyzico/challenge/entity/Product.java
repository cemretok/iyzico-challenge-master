package com.iyzico.challenge.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;

@Entity
public class Product {

	@Id
	@GeneratedValue
	private Long id;
	private String productName;
	private String productDetail;
	private BigDecimal productPrice;
	private int productCount;

	@Version
	private Integer version;

	public Product() {
		super();
	}

	public Product(Long id, String productName, String productDetail, BigDecimal productPrice, int productCount) {
		super();
		this.id = id;
		this.productName = productName;
		this.productDetail = productDetail;
		this.productPrice = productPrice;
		this.productCount = productCount;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductDetail() {
		return productDetail;
	}

	public void setProductDetail(String productDetail) {
		this.productDetail = productDetail;
	}

	public BigDecimal getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(BigDecimal productPrice) {
		this.productPrice = productPrice;
	}

	public int getProductCount() {
		return productCount;
	}

	public void setProductCount(int productCount) {
		this.productCount = productCount;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

}
