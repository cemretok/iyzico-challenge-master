package com.iyzico.challenge.service;

import java.util.List;

import com.iyzico.challenge.entity.Basket;

public interface BasketService {
	String addToBasket(Basket basket);

	void deleteFromBasket(Long id);

	Basket findBasket(Long id);

	List<Basket> findAllBasket();
	
	void payBasket(Basket basket);

}
