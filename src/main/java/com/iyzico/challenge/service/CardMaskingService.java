package com.iyzico.challenge.service;

public class CardMaskingService {

	public String maskCardNumber(String cardNumber) {
		StringBuffer maskedCard = new StringBuffer(cardNumber);

		int totalLength = maskedCard.length();
		int startLength = 6;
		int endLength = 4;
		int[] validDigitLength = { 15, 16 };
		int startPoint = 0;
		int endPoint = 0;
		int digitCount = 0;

		for (int i = 0; i < totalLength; i++) {
			if (Character.isDigit(maskedCard.charAt(i))) {
				if (0 == startLength--) {
					startPoint = i;
				}
				digitCount++;
			}

			if (Character.isDigit(maskedCard.charAt(totalLength - i - 1))) {
				if (0 == endLength--) {
					endPoint = totalLength - i;
				}
			}
		}

		for (int v : validDigitLength) {
			if (v == digitCount) {
				for (int i = startPoint; i < endPoint; i++) {
					if (Character.isDigit(maskedCard.charAt(i))) {
						maskedCard.replace(i, i + 1, "*");

					}
				}
			}
		}

		System.gc();
		return maskedCard.toString();
	}

}
