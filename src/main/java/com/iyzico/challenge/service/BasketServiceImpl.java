package com.iyzico.challenge.service;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.iyzico.challenge.entity.Basket;
import com.iyzico.challenge.entity.Product;
import com.iyzico.challenge.repository.BasketRepository;

@Service
public class BasketServiceImpl implements BasketService {

	private Logger logger = LoggerFactory.getLogger(BasketServiceImpl.class);

	private BasketRepository basketRepository;

	private ProductService productService;

	private IyzicoPaymentService paymentService;

	public BasketServiceImpl(BasketRepository basketRepository, ProductService productService,
			IyzicoPaymentService paymentService) {
		this.basketRepository = basketRepository;
		this.productService = productService;
		this.paymentService = paymentService;
	}

	@Override
	public String addToBasket(Basket basket) {
		String status = null;
		for (Product p : basket.getProduct()) {
			int productCount = p.getProductCount();

			if (productCount >= 1) {
				p.setProductCount(p.getProductCount() - 1);
				productService.updateProduct(p);
				basketRepository.save(basket);

				logger.info("addToBasket successfully!");
				status = "Added to basket!";
			} else {
				logger.info("addToBasket has no stock!!");
				status = "Not added to basket!";
			}
		}

		return status;
	}

	@Override
	public void deleteFromBasket(Long id) {
		basketRepository.deleteById(id);
		logger.info("deleteFromBasket successfully!");
	}

	@Override
	public Basket findBasket(Long id) {
		return basketRepository.findById(id).orElse(null);
	}

	@Override
	public List<Basket> findAllBasket() {
		return basketRepository.findAll();
	}

	@Override
	public void payBasket(Basket basket) {
		BigDecimal price = BigDecimal.ZERO;
		for (Product p : basket.getProduct()) {
			price.add(p.getProductPrice());
		}
		if (price.compareTo(BigDecimal.ZERO) > 0) {
			paymentService.pay(price);
			logger.info("payBasket successfully!");
		}
	}
}
