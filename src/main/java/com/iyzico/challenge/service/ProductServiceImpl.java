package com.iyzico.challenge.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iyzico.challenge.entity.Product;
import com.iyzico.challenge.repository.ProductRepository;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

	private ProductRepository productRepository;

	public ProductServiceImpl(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	@Override
	public void addProduct(Product product) {
		productRepository.save(product);
	}

	@Override
	public void deleteProduct(Product product) {
		productRepository.delete(product);
	}

	@Override
	public void updateProduct(Product product) {
		Product productFromDB = productRepository.getOne(product.getId());

		productFromDB.setProductCount(product.getProductCount());
		productFromDB.setProductPrice(product.getProductPrice());

		productRepository.save(productFromDB);
	}

	@Override
	public List<Product> findAllProduct() {
		return productRepository.findAll();
	}

	@Override
	public Product findOneProduct(Long id) {
		return productRepository.findById(id).orElse(null);
	}
}
