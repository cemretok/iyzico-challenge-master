package com.iyzico.challenge.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.iyzico.challenge.entity.Payment;
import com.iyzico.challenge.repository.PaymentRepository;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;

@Service
@Transactional
public class IyzicoPaymentService {

	private Logger logger = LoggerFactory.getLogger(IyzicoPaymentService.class);

	private BankService bankService;
	private PaymentRepository paymentRepository;

	static List<Payment> payments = new ArrayList<>();

	public IyzicoPaymentService(BankService bankService, PaymentRepository paymentRepository) {
		this.bankService = bankService;
		this.paymentRepository = paymentRepository;
	}

	public void pay(BigDecimal price) {

		Observable.fromCallable(new Callable<BankPaymentResponse>() {

			@Override
			public BankPaymentResponse call() throws Exception {
				// pay with bank
				BankPaymentRequest request = new BankPaymentRequest();
				request.setPrice(price);
				BankPaymentResponse bankPaymentResponse = bankService.pay(request);
				logger.info("BankPaymentResponse called successfully!");
				return bankPaymentResponse;
			}
		}).subscribe(new Consumer<BankPaymentResponse>() {

			@Override
			public void accept(BankPaymentResponse r) throws Exception {
				// insert records
				Payment payment = new Payment();
				payment.setBankResponse(r.getResultCode());
				payment.setPrice(price);
				paymentRepository.save(payment);

				logger.info("Payment saved successfully!");
			}
		});
	}
}
