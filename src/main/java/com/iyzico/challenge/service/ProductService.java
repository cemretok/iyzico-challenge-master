package com.iyzico.challenge.service;

import java.util.List;

import com.iyzico.challenge.entity.Product;

public interface ProductService {
	void addProduct(Product product);

	void deleteProduct(Product product);

	void updateProduct(Product product);

	List<Product> findAllProduct();

	Product findOneProduct(Long id);
}
