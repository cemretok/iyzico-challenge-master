package com.iyzico.challenge.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.iyzico.challenge.entity.Basket;

public interface BasketRepository extends JpaRepository<Basket, Long> {
}
