package com.iyzico.challenge.configuration;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = { "com.iyzico.challenge" })
@EntityScan(basePackages = { "com.iyzico.challenge" })
@EnableTransactionManagement
public class ChallengeConfiguration {

}
