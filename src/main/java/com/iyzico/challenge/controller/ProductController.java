package com.iyzico.challenge.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.iyzico.challenge.entity.Product;
import com.iyzico.challenge.service.ProductServiceImpl;

@RestController
@RequestMapping("/product")
public class ProductController {
	private static final Logger log = LoggerFactory.getLogger(ProductController.class);

	private ProductServiceImpl productService;

	public ProductController(ProductServiceImpl productService) {
		this.productService = productService;
	}

	@GetMapping(value = "/allproduct", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getAllProduct() {
		log.info("getAllProduct function called.");
		return new ResponseEntity<Object>(productService.findAllProduct(), HttpStatus.OK);
	}

	@GetMapping(value = "/oneproduct/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getOneProduct(@PathVariable("id") Long id) {
		log.info("getOneProduct function called.");
		return new ResponseEntity<Object>(productService.findOneProduct(id), HttpStatus.OK);
	}

	@PostMapping(value = "/addproduct", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> addProduct(@RequestBody Product product) {
		log.info("addProduct function called.");
		productService.addProduct(product);
		return new ResponseEntity<Object>("Successfully Added", new HttpHeaders(), HttpStatus.OK);
	}

	@DeleteMapping(value = "/deleteproduct", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> deleteProduct(@RequestBody Product product) {
		log.info("deleteProduct function called.");
		productService.deleteProduct(product);
		return new ResponseEntity<Object>("Successfully Deleted", new HttpHeaders(), HttpStatus.OK);
	}

	@PostMapping(value = "/updateproduct", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> updateProduct(@RequestBody Product product) {
		log.info("updateProduct function called.");
		productService.updateProduct(product);
		return new ResponseEntity<Object>("Successfully Updated", new HttpHeaders(), HttpStatus.OK);
	}
}
