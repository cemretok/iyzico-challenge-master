package com.iyzico.challenge.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.iyzico.challenge.entity.Basket;
import com.iyzico.challenge.service.BasketServiceImpl;

@RestController
@RequestMapping("/basket")
public class BasketController {
	private static final Logger log = LoggerFactory.getLogger(BasketController.class);

	private BasketServiceImpl basketService;

	public BasketController(BasketServiceImpl basketService) {
		this.basketService = basketService;
	}

	@PostMapping(value = "/addbasket", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> addBasket(@RequestBody Basket basket) {
		log.info("addBasket function called.");
		basketService.addToBasket(basket);
		return new ResponseEntity<Object>("Successfully Added", new HttpHeaders(), HttpStatus.OK);
	}

	@DeleteMapping(value = "/deletebasket/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> deleteBasket(@PathVariable("id") Long id) {
		log.info("deleteBasket function called.");
		basketService.deleteFromBasket(id);
		return new ResponseEntity<Object>("Successfully Deleted", new HttpHeaders(), HttpStatus.OK);
	}

	@GetMapping(value = "/getbasket/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getBasket(@PathVariable("id") Long id) {
		log.info("getBasket function called.");
		return new ResponseEntity<Object>(basketService.findBasket(id), HttpStatus.OK);
	}

	@GetMapping(value = "/getallbasket", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getAllBasket() {
		log.info("getAllBasket function called.");
		return new ResponseEntity<Object>(basketService.findAllBasket(), HttpStatus.OK);
	}

	@PostMapping(value = "/paybasket", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> payBasket(@RequestBody Basket basket) {
		log.info("payBasket function called.");

		return new ResponseEntity<Object>(basketService.findBasket(basket.getId()), HttpStatus.OK);
	}
}
