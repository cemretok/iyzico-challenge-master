package com.iyzico.challenge.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.iyzico.challenge.controller.ProductController;
import com.iyzico.challenge.entity.Product;
import com.iyzico.challenge.repository.ProductRepository;

@EnableAutoConfiguration
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceTest {

	@Mock
	@Autowired
	public ProductRepository productRepository;

	@Autowired
	public ProductServiceImpl productService;

	@Autowired
	public ProductController productController;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void add_product_and_count_test() {
		// given

		int size = productService.findAllProduct().size();

		Product product1 = new Product();
		product1.setProductCount(10);
		product1.setProductDetail("Detay");
		product1.setProductName("Name");
		product1.setProductPrice(new BigDecimal("0.02"));

		Product product2 = new Product();
		product2.setProductCount(10);
		product2.setProductDetail("Detay");
		product2.setProductName("Name");
		product2.setProductPrice(new BigDecimal("0.02"));

		// when
		productService.addProduct(product1);
		productService.addProduct(product2);

		// then
		assertThat(size + 2).isEqualTo(productService.findAllProduct().size());
	}

	@Test
	public void delete_product_and_count_test() {
		// given

		Product product1 = new Product();
		product1.setProductCount(10);
		product1.setProductDetail("Detay");
		product1.setProductName("Name");
		product1.setProductPrice(new BigDecimal("0.02"));

		Product product2 = new Product();
		product2.setProductCount(10);
		product2.setProductDetail("Detay");
		product2.setProductName("Name");
		product2.setProductPrice(new BigDecimal("0.02"));

		// when
		productService.addProduct(product1);
		productService.addProduct(product2);

		int oldSize = productService.findAllProduct().size();

		productService.deleteProduct(product1);
		productService.deleteProduct(product2);

		// then
		assertThat(oldSize - 2).isEqualTo(productService.findAllProduct().size());
	}

	@Test
	public void find_one_product_test() {
		// given

		Product product1 = new Product();
		product1.setProductCount(10);
		product1.setProductDetail("Bir");
		product1.setProductName("Name");
		product1.setProductPrice(new BigDecimal("0.02"));

		Product product2 = new Product();
		product2.setProductCount(10);
		product2.setProductDetail("İki");
		product2.setProductName("Name");
		product2.setProductPrice(new BigDecimal("0.02"));

		productService.addProduct(product1);
		productService.addProduct(product2);

		// when

		List<Product> result = productService.findAllProduct().stream()
				.filter(line -> "Bir".equals(line.getProductDetail())).collect(Collectors.toList());

		assertThat(productService.findOneProduct(result.get(0).getId()).getProductDetail()).isEqualTo("Bir");

	}

	@Test
	public void mock_find_all_product_test() {
		// given
		Product product1 = new Product();
		product1.setProductCount(10);
		product1.setProductDetail("Bir");
		product1.setProductName("Name");
		product1.setProductPrice(new BigDecimal("0.02"));

		Product product2 = new Product();
		product2.setProductCount(10);
		product2.setProductDetail("İki");
		product2.setProductName("Name");
		product2.setProductPrice(new BigDecimal("0.02"));

		productService.addProduct(product1);
		productService.addProduct(product2);

		// when
		when(productRepository.findAll()).thenReturn(productService.findAllProduct());

		// Then
		assertEquals(productRepository.findAll().size(), productService.findAllProduct().size());

	}

	@Test
	public void is_addProduct_return_success() {
		// given
		Product product = new Product();
		product.setProductCount(10);
		product.setProductDetail("Product2");
		product.setProductName("ProductName2");
		product.setProductPrice(new BigDecimal("1"));

		// When
		productService.addProduct(product);

		// Then
		assertThat(productController.addProduct(product).getBody()).isEqualTo("Successfully Added");
	}

	@Test
	public void is_deleteProduct_return_success() {
		// given
		Product product = new Product();
		product.setProductCount(10);
		product.setProductDetail("Product2");
		product.setProductName("ProductName2");
		product.setProductPrice(new BigDecimal("1"));

		// When
		productService.addProduct(product);
		Product productObject = productService.findAllProduct().get(0);

		// Then
		assertThat(productController.deleteProduct(productObject).getBody()).isEqualTo("Successfully Deleted");
	}

	@Test
	public void is_updateProduct_return_success() {
		// given
		Product product = new Product();
		product.setProductCount(10);
		product.setProductDetail("Product2");
		product.setProductName("ProductName2");
		product.setProductPrice(new BigDecimal("1"));

		// When
		productService.addProduct(product);
		Product productObject = productService.findAllProduct().get(0);
		productObject.setProductName("Product3");

		// Then
		assertThat(productController.updateProduct(productObject).getBody()).isEqualTo("Successfully Updated");
	}

	@Test
	public void is_findallproduct_equal_to_size() {
		// given
		int productSize = productService.findAllProduct().size();

		// When

		// Then
		assertThat(((List<Product>) productController.getAllProduct().getBody()).size()).isEqualTo(productSize);
	}

	@Test
	public void is_findoneproduct_equal_to_product() {
		// Given
		Product product = new Product();
		product.setProductCount(10);
		product.setProductDetail("Product10");
		product.setProductName("ProductName10");
		product.setProductPrice(new BigDecimal("1"));

		productService.addProduct(product);

		// When
		Product productObject = productService.findAllProduct().get(0);

		// Then
		assertThat(((Product) productController.getOneProduct(productObject.getId()).getBody()).getProductName())
				.isEqualTo(productObject.getProductName());
	}
}
