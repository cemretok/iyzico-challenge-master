package com.iyzico.challenge.service;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

//TODO: add new test cases for edge cases
public class CardMaskingServiceTest {

	private CardMaskingService cardMaskingService;

	@Before
	public void setUp() {
		cardMaskingService = new CardMaskingService();
	}

	@Test
	public void should_mask_digits_for_basic_credit_cards() {
		// given
		String cardNumber = "4729150000000005";

		// when
		String maskedCardNumber = cardMaskingService.maskCardNumber(cardNumber);
		System.out.println(maskedCardNumber);
		// then
		assertThat(maskedCardNumber).isEqualTo("472915******0005");
	}

	@Test
	public void should_mask_digits_for_space_credit_cards() {
		// given
		String cardNumber = "4729 1500 0000 0005";

		// when
		String maskedCardNumber = cardMaskingService.maskCardNumber(cardNumber);
		System.out.println(maskedCardNumber);
		// then
		assertThat(maskedCardNumber).isEqualTo("4729 15** **** 0005");
	}

	@Test
	public void should_mask_digits_for_null_credit_cards() {
		// given
		String cardNumber = "";

		// when
		String maskedCardNumber = cardMaskingService.maskCardNumber(cardNumber);
		System.out.println(maskedCardNumber);
		// then
		assertThat(maskedCardNumber).isEqualTo("");
		//System.gc();
	}

	@Test
	public void should_mask_digits_for_minus_basic_credit_cards() {
		// given
		String cardNumber = "472915000000005";

		// when
		String maskedCardNumber = cardMaskingService.maskCardNumber(cardNumber);
		System.out.println(maskedCardNumber);

		// then
		assertThat(maskedCardNumber).isEqualTo("472915*****0005");
	}

	@Test
	public void should_mask_digits_for_plus_basic_credit_cards() {
		// given
		String cardNumber = "47291500000000005";

		// when
		String maskedCardNumber = cardMaskingService.maskCardNumber(cardNumber);
		System.out.println(maskedCardNumber);

		// then
		assertThat(maskedCardNumber).isEqualTo("47291500000000005");
	}

	@Test
	public void should_mask_digits_for_credit_cards_in_different_format() {
		// given
		String cardNumber = "4729-1500-0000-0005";

		// when
		String maskedCardNumber = cardMaskingService.maskCardNumber(cardNumber);
		System.out.println(maskedCardNumber);

		// then
		assertThat(maskedCardNumber).isEqualTo("4729-15**-****-0005");
		System.gc();
	}

	@Test
	public void should_mask_digits_for_credit_cards_in_plus_different_format() {
		// given
		String cardNumber = "4729-1500-0000-00005";

		// when
		String maskedCardNumber = cardMaskingService.maskCardNumber(cardNumber);
		System.out.println(maskedCardNumber);

		// then
		assertThat(maskedCardNumber).isEqualTo("4729-1500-0000-00005");
	}

	@Test
	public void should_mask_digits_for_credit_cards_in_minus_different_format() {
		// given
		String cardNumber = "4729-1500-0000-005";

		// when
		String maskedCardNumber = cardMaskingService.maskCardNumber(cardNumber);
		System.out.println(maskedCardNumber);

		// then
		assertThat(maskedCardNumber).isEqualTo("4729-15**-***0-005");
	}

	@Test
	public void should_mask_digits_for_credit_cards_in_param_different_format() {
		// given
		String cardNumber = "4729-1500-0000-005-";

		// when
		String maskedCardNumber = cardMaskingService.maskCardNumber(cardNumber);
		System.out.println(maskedCardNumber);

		// then
		assertThat(maskedCardNumber).isEqualTo("4729-15**-***0-005-");
	}

	@Test
	public void should_not_mask_anything_for_non_numeric_characters() {
		// given
		String cardNumber = "John Doe";

		// when
		String maskedCardNumber = cardMaskingService.maskCardNumber(cardNumber);
		System.out.println(maskedCardNumber);

		// then
		assertThat(maskedCardNumber).isEqualTo("John Doe");
	}

	@Test
	public void should_not_mask_anything_for_alfa_numeric_characters() {
		// given
		String cardNumber = "John Doe 1234545a6d712345";

		// when
		String maskedCardNumber = cardMaskingService.maskCardNumber(cardNumber);
		System.out.println(maskedCardNumber);

		// then
		assertThat(maskedCardNumber).isEqualTo("John Doe 1234545a6d712345");
	}
}
