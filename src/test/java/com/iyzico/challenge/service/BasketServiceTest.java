package com.iyzico.challenge.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.annotation.Async;
import org.springframework.test.context.junit4.SpringRunner;

import com.iyzico.challenge.controller.BasketController;
import com.iyzico.challenge.controller.ProductController;
import com.iyzico.challenge.entity.Basket;
import com.iyzico.challenge.entity.Payment;
import com.iyzico.challenge.entity.Product;
import com.iyzico.challenge.repository.BasketRepository;
import com.iyzico.challenge.repository.PaymentRepository;
import com.iyzico.challenge.repository.ProductRepository;

@EnableAutoConfiguration
@RunWith(SpringRunner.class)
@SpringBootTest
public class BasketServiceTest {

	@Mock
	@Autowired
	public BasketRepository basketRepository;

	@Autowired
	public BasketServiceImpl basketService;

	@Autowired
	public BasketController basketController;

	@Mock
	@Autowired
	public ProductRepository productRepository;

	@InjectMocks
	@Autowired
	public ProductServiceImpl productService;

	@Autowired
	public ProductController productController;

	@Autowired
	private BankService bankService;

	@Mock
	@Autowired
	private PaymentRepository paymentRepository;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void add_product_to_basket_and_count_test() {
		// given
		String nameConcat = "";

		Product product1 = new Product();
		product1.setProductCount(10);
		product1.setProductDetail("Detay1");
		product1.setProductName("Name1");
		product1.setProductPrice(new BigDecimal("0.01"));

		Product product2 = new Product();
		product2.setProductCount(10);
		product2.setProductDetail("Detay2");
		product2.setProductName("Name2");
		product2.setProductPrice(new BigDecimal("0.02"));

		Product product3 = new Product();
		product3.setProductCount(10);
		product3.setProductDetail("Detay3");
		product3.setProductName("Name3");
		product3.setProductPrice(new BigDecimal("0.03"));

		Basket basket = new Basket();
		basket.setProduct(Arrays.asList(product1, product2, product3));

		// when
		productService.addProduct(product1);
		productService.addProduct(product2);
		productService.addProduct(product3);
		basketService.addToBasket(basket);

		for (Basket b : basketService.findAllBasket()) {
			for (Product p : b.getProduct()) {
				if (b.getProduct().size() == 3) {
					nameConcat += p.getProductName();
				}
			}
		}

		// then
		assertThat(nameConcat).isEqualTo("Name1Name2Name3");
	}

	@Test
	public void delete_product_and_count_test() {
		// given

		int size = productService.findAllProduct().size();

		Product product1 = new Product();
		product1.setProductCount(10);
		product1.setProductDetail("Detay1");
		product1.setProductName("Name1");
		product1.setProductPrice(new BigDecimal("0.02"));

		Product product2 = new Product();
		product2.setProductCount(10);
		product2.setProductDetail("Detay2");
		product2.setProductName("Name2");
		product2.setProductPrice(new BigDecimal("0.02"));

		Basket basket = new Basket();
		basket.setProduct(Arrays.asList(product1, product2));

		// when
		productService.addProduct(product1);
		productService.addProduct(product2);
		basketService.addToBasket(basket);

		// basketService.deleteFromBasket(Integer.toUnsignedLong(1));

		// then
		// assertThat(basketService.findAllBasket().size()).isEqualTo(0);
	}

	@Test
	public void add_limited_product_more_than_one_basket_test() {
		// given

		int productSize = 100;
		int overSized = 100;

		Product product = new Product();
		product.setProductCount(productSize);
		product.setProductDetail("Product");
		product.setProductName("ProductName");
		product.setProductPrice(new BigDecimal("0.02"));

		productService.addProduct(product);

		// when
		List<CompletableFuture<String>> futures = new ArrayList<>();
		for (int i = 0; i < productSize + overSized; i++) {
			CompletableFuture<String> future = call(product);
			futures.add(future);
		}

		futures.stream().forEach(f -> CompletableFuture.allOf(f).join());

		List<Basket> result = basketService.findAllBasket().stream()
				.filter(line -> "ProductName".equals(line.getProduct().get(0).getProductName()))
				.collect(Collectors.toList());

		// then
		assertThat(result.size()).isEqualTo(productSize);
	}

	@Async
	public CompletableFuture<String> call(Product product) {
		Basket basket = new Basket();
		basket.setProduct(Arrays.asList(product));

		return CompletableFuture.completedFuture(basketService.addToBasket(basket));
	}

	@Test
	public void is_payment_equal_to_basket() {
		// given

		List<Payment> payment = paymentRepository.findAll();

		int productSize = 100;
		int overSized = 100;

		Product product = new Product();
		product.setProductCount(productSize);
		product.setProductDetail("Product2");
		product.setProductName("ProductName2");
		product.setProductPrice(new BigDecimal("1"));

		productService.addProduct(product);

		// when
		List<CompletableFuture<String>> futures = new ArrayList<>();
		for (int i = 0; i < productSize + overSized; i++) {
			CompletableFuture<String> future = call(product);
			futures.add(future);
		}

		futures.stream().forEach(f -> CompletableFuture.allOf(f).join());

		List<Basket> result = basketService.findAllBasket().stream()
				.filter(line -> "ProductName2".equals(line.getProduct().get(0).getProductName()))
				.collect(Collectors.toList());

		int basketSum = result.stream().mapToInt(x -> x.getProduct().get(0).getProductPrice().intValue()).sum();

		// then
		assertThat(basketSum).isEqualTo(100);

	}

	@Test
	public void is_addBasket_return_success() {
		// given
		Product product = new Product();
		product.setProductCount(10);
		product.setProductDetail("Product2");
		product.setProductName("ProductName2");
		product.setProductPrice(new BigDecimal("1"));

		Basket basket = new Basket();

		// When
		productService.addProduct(product);
		basket.setProduct(Arrays.asList(product));

		// Then
		assertThat(basketController.addBasket(basket).getBody()).isEqualTo("Successfully Added");
	}

	@Test
	public void is_deleteBasket_return_success() {
		// Given
		Product product = new Product();
		product.setProductCount(10);
		product.setProductDetail("Product2");
		product.setProductName("ProductName2");
		product.setProductPrice(new BigDecimal("1"));

		Basket basket = new Basket();

		// When
		productService.addProduct(product);
		basket.setProduct(Arrays.asList(product));

		Basket basketObject = basketService.findAllBasket().get(0);

		// Then
		assertThat(basketController.deleteBasket(basketObject.getId()).getBody()).isEqualTo("Successfully Deleted");
	}

	@Test
	public void is_findallbasket_equal_to_size() {
		// given
		int productSize = basketService.findAllBasket().size();

		// When

		// Then
		assertThat(((List<Basket>) basketController.getAllBasket().getBody()).size()).isEqualTo(productSize);
	}

	@Test
	public void is_findonebasket_equal_to_basket() {
		// Given
		Product product = new Product();
		product.setProductCount(10);
		product.setProductDetail("Product2");
		product.setProductName("ProductName2");
		product.setProductPrice(new BigDecimal("1"));

		Basket basket = new Basket();

		// When
		productService.addProduct(product);
		basket.setProduct(Arrays.asList(product));

		Basket basketObject = basketService.findAllBasket().get(0);

		// Then
		assertThat(((Basket) basketController.getBasket(basketObject.getId()).getBody()).getId())
				.isEqualTo(basketObject.getId());
	}
}
